package com.paurus.students.interceptors;

import com.paurus.students.entities.Student;
import com.paurus.students.exceptions.UnauthorizedException;
import com.paurus.students.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuthService authService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws UnauthorizedException {
        String userAuthHash = request.getHeader("AuthHash");

        Student authStudent = authService.getAuthorizedStudent(userAuthHash);

        if(userAuthHash == null || userAuthHash.isBlank() || authStudent == null) {
            throw new UnauthorizedException("Access denied!");
        }

        request.setAttribute("authorizedStudentId", authStudent.getId());

        return true;
    }
}
