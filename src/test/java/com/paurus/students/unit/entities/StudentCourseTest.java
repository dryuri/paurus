package com.paurus.students.unit.entities;

import com.paurus.students.entities.Course;
import com.paurus.students.entities.Student;
import com.paurus.students.entities.StudentCourse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class StudentCourseTest {

    @Test
    public void testConstructorAndGetters() throws NoSuchFieldException, IllegalAccessException {
        StudentCourse studentCourse = EntityGenerator.createTestStudentCourse();

        assertEquals(EntityGenerator.STUDENT_COURSE_ID, studentCourse.getId());
        assertEquals(EntityGenerator.createTestCourse().getName(), studentCourse.getCourse().getName());
        assertEquals(EntityGenerator.createTestStudent().getName(), studentCourse.getStudent().getName());
    }

    @Test
    public void testSetters() throws NoSuchFieldException, IllegalAccessException {
        StudentCourse studentCourse = EntityGenerator.createTestStudentCourse();

        Course course = studentCourse.getCourse();
        course.setName(EntityGenerator.COURSE_NAME_EDIT);

        Student student = studentCourse.getStudent();
        student.setName(EntityGenerator.STUDENT_NAME_EDIT);

        studentCourse.setCourse(course);
        studentCourse.setStudent(student);

        assertEquals(course.getName(), studentCourse.getCourse().getName());
        assertEquals(student.getName(), studentCourse.getStudent().getName());
    }
}
