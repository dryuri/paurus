package com.paurus.students.services;

import com.paurus.students.entities.Course;
import com.paurus.students.entities.Student;
import com.paurus.students.entities.StudentCourse;
import com.paurus.students.exceptions.ResourceNotFoundException;
import com.paurus.students.repositories.CourseRepository;
import com.paurus.students.repositories.StudentCourseRepository;
import com.paurus.students.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.data.jpa.domain.Specification.where;

@Service("SchoolServiceImpl")
public class SchoolServiceImpl implements SchoolService {

    private final StudentRepository studentRepository;

    private CourseRepository courseRepository;

    private StudentCourseRepository studentCourseRepository;

    @Autowired
    public SchoolServiceImpl(CourseRepository courseRepository, StudentCourseRepository studentCourseRepository, StudentRepository studentRepository) {
        this.courseRepository = courseRepository;
        this.studentCourseRepository = studentCourseRepository;
        this.studentRepository = studentRepository;
    }

    @Cacheable(value = "coursesCache")
    @Override
    public List<Course> getAllCourses(String searchTerm) {
        return this.courseRepository.findAll(nameContains(searchTerm));
    }

    @Transactional
    @Override
    public Student enrollStudentToCourse(Long studentId, Long courseId) {
        if(this.studentCourseRepository.existsByStudentIdAndCourseId(studentId, courseId)) {
            return this.studentRepository.findById(studentId).orElseThrow(this::studentNotFoundException);
        }

        Student student = this.studentRepository.findById(studentId).orElseThrow(this::studentNotFoundException);
        Course course = this.courseRepository.findById(courseId).orElseThrow(this::courseNotFoundException);

        this.studentCourseRepository.save(new StudentCourse(student, course));

        return this.studentRepository.findById(studentId).orElseThrow(this::studentNotFoundException);
    }

    @Transactional
    @Override
    public Student removeStudentFromCourse(Long studentId, Long courseId) {
        this.studentCourseRepository.deleteByStudentIdAndCourseId(studentId, courseId);

        return this.studentRepository.findById(studentId).orElseThrow(this::studentNotFoundException);
    }

    private ResourceNotFoundException studentNotFoundException() {
        return new ResourceNotFoundException("Student not found");
    }

    private ResourceNotFoundException courseNotFoundException() {
        return new ResourceNotFoundException("Course not found");
    }

    private static Specification<Course> nameContains(String searchTerm) {
        return (course, query, criteriaBuilder) -> criteriaBuilder.like(course.get("name"), "%" + searchTerm + "%");
    }
}
