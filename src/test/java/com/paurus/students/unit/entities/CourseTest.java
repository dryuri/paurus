package com.paurus.students.unit.entities;

import com.paurus.students.entities.Course;
import com.paurus.students.entities.StudentCourse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CourseTest {

    @Test
    public void testConstructorAndGetters() throws NoSuchFieldException, IllegalAccessException {
        Course course = EntityGenerator.createTestCourse();

        assertEquals(EntityGenerator.COURSE_ID, course.getId());
        assertEquals(EntityGenerator.COURSE_NAME, course.getName());
        assertEquals(new HashSet<StudentCourse>(), course.getStudentCourses());
    }

    @Test
    public void testSetters() throws NoSuchFieldException, IllegalAccessException {
        Course course = EntityGenerator.createTestCourse();

        assertEquals(0, course.getStudentCourses().size());

        course.setName(EntityGenerator.COURSE_NAME_EDIT);
        course.setStudentCourses(EntityGenerator.createTestStudentCourseSet());

        assertEquals(EntityGenerator.COURSE_NAME_EDIT, course.getName());
        assertEquals(1, course.getStudentCourses().size());
    }
}
