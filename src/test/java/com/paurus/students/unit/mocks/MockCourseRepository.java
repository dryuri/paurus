package com.paurus.students.unit.mocks;

import com.paurus.students.entities.Course;
import com.paurus.students.repositories.CourseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class MockCourseRepository implements CourseRepository {

    private Map<Long, Course> courses;

    public MockCourseRepository() {
        this.courses = new HashMap<>();
    }

    @Override
    public Optional<Course> findOne(Specification<Course> specification) {
        return Optional.empty();
    }

    @Override
    public Optional<Course> findById(Long aLong) {
        return Optional.ofNullable(this.courses.get(aLong));
    }

    @Override
    public List<Course> findAll(Specification<Course> specification) {
        return new ArrayList<>(this.courses.values());
    }

    @Override
    public Iterable<Course> findAll() {
        return new ArrayList<>(this.courses.values());
    }

    @Override
    public Page<Course> findAll(Specification<Course> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<Course> findAll(Specification<Course> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Course> specification) {
        return 0;
    }

    @Override
    public Iterable<Course> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Course> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Course> S save(S s) {
        this.courses.put(s.getId(), s);
        return null;
    }

    @Override
    public <S extends Course> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Course> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Course course) {

    }

    @Override
    public void deleteAll(Iterable<? extends Course> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
