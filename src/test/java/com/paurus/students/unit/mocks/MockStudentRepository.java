package com.paurus.students.unit.mocks;

import com.paurus.students.entities.Student;
import com.paurus.students.repositories.StudentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MockStudentRepository implements StudentRepository {

    private Map<Long, Student> students;

    public MockStudentRepository() {
        this.students = new HashMap<>();
    }

    @Override
    public <S extends Student> S save(S s) {
        this.students.put(s.getId(), s);
        return s;
    }

    @Override
    public Optional<Student> findById(Long aLong) {
        return Optional.ofNullable(this.students.get(aLong));
    }

    @Override
    public Student findByAuthHash(String authHash) {
        return null;
    }

    @Override
    public Iterable<Student> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Student> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Student> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Student> findAll() {
        return null;
    }

    @Override
    public Iterable<Student> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Student student) {

    }

    @Override
    public void deleteAll(Iterable<? extends Student> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
