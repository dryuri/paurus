package com.paurus.students.controllers;

import com.paurus.students.dtos.CourseDto;
import com.paurus.students.dtos.StudentDto;
import com.paurus.students.entities.Course;
import com.paurus.students.entities.Student;
import com.paurus.students.entities.StudentCourse;
import com.paurus.students.services.SchoolService;
import org.hibernate.validator.constraints.Length;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Validated
public class CourseController {

    private SchoolService schoolService;

    private ModelMapper modelMapper;

    @Autowired
    public CourseController(@Qualifier("SchoolServiceImpl") SchoolService schoolService, ModelMapper modelMapper) {
        this.schoolService = schoolService;
        this.modelMapper = modelMapper;
    }

    // TODO possible improvement - Add pagination
    @GetMapping("/courses")
    public List<CourseDto> getAllCourses(@RequestParam(required = false, defaultValue = "", name = "q") @Length(max = 50, message = "q parameter length must be between 0 and 50") String searchTerm) {
        List<Course> courses = this.schoolService.getAllCourses(searchTerm);
        return courses.stream().map(
                this::convertCourseToDto
        ).collect(Collectors.toList());
    }

    @PostMapping("/courses/{courseId}/enroll")
    public StudentDto enrollToCourse(@RequestAttribute("authorizedStudentId") Long studentId, @PathVariable long courseId) {
        return this.convertStudentToDto(this.schoolService.enrollStudentToCourse(studentId, courseId));
    }

    @PostMapping("/courses/{courseId}/leave")
    public StudentDto leaveCourse(@RequestAttribute("authorizedStudentId") Long studentId, @PathVariable long courseId) {
        return this.convertStudentToDto(this.schoolService.removeStudentFromCourse(studentId, courseId));
    }

    private CourseDto convertCourseToDto(Course course) {
        return this.modelMapper.map(course, CourseDto.class);
    }

    private StudentDto convertStudentToDto(Student student) {
        StudentDto studentDto = this.modelMapper.map(student, StudentDto.class);

        for(StudentCourse studentCourse : student.getCourses()) {
            studentDto.addCourse(this.convertCourseToDto(studentCourse.getCourse()));
        }

        return studentDto;
    }
}
