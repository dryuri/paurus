package com.paurus.students.unit.services;

import com.paurus.students.entities.Course;
import com.paurus.students.repositories.CourseRepository;
import com.paurus.students.repositories.StudentCourseRepository;
import com.paurus.students.repositories.StudentRepository;
import com.paurus.students.services.SchoolServiceImpl;
import com.paurus.students.unit.entities.EntityGenerator;
import com.paurus.students.unit.mocks.MockCourseRepository;
import com.paurus.students.unit.mocks.MockStudentCourseRepository;
import com.paurus.students.unit.mocks.MockStudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class SchoolServiceTest {

    @Spy
    private CourseRepository courseRepository = new MockCourseRepository();

    @Spy
    private StudentRepository studentRepository = new MockStudentRepository();

    @Spy
    private StudentCourseRepository studentCourseRepository = new MockStudentCourseRepository();

    @InjectMocks
    private SchoolServiceImpl schoolService;

    @BeforeEach
    void init() {
        initMocks(this);
        this.schoolService = new SchoolServiceImpl(this.courseRepository, this.studentCourseRepository, this.studentRepository);
    }

    @Test
    public void getAllCourses() {
        this.courseRepository.save(new Course("course1"));
        this.courseRepository.save(new Course("course2"));

        var actual = this.schoolService.getAllCourses(null);

        assertEquals(this.courseRepository.findAll(), actual);
    }

    @Test
    public void testEnrollToCourse() throws NoSuchFieldException, IllegalAccessException {
        this.courseRepository.save(EntityGenerator.createTestCourse(EntityGenerator.COURSE_ID, "enroll"));
        this.studentRepository.save(EntityGenerator.createTestStudent());

        this.schoolService.enrollStudentToCourse(EntityGenerator.STUDENT_ID, EntityGenerator.COURSE_ID);

        verify(this.studentCourseRepository, times(1)).existsByStudentIdAndCourseId(any(), any());
    }
}
