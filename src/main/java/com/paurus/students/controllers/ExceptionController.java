package com.paurus.students.controllers;

import com.paurus.students.exceptions.ResourceNotFoundException;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExceptionController implements ErrorController {

    @RequestMapping("/error")
    public String handleError() {
        throw new ResourceNotFoundException("Resource not found");
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
