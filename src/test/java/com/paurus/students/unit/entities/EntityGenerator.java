package com.paurus.students.unit.entities;

import com.paurus.students.entities.Course;
import com.paurus.students.entities.Student;
import com.paurus.students.entities.StudentCourse;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class EntityGenerator {

    public static final Long STUDENT_ID = 456L;
    public static final String STUDENT_NAME = "Student1337";
    public static final String STUDENT_NAME_EDIT = "new student";
    public static final String STUDENT_AUTH_HASH = "login-hash";
    public static final String STUDENT_AUTH_HASH_EDIT = "edited hash";

    public static final String COURSE_NAME = "my test course";
    public static final String COURSE_NAME_EDIT = "edited";
    public static final Long COURSE_ID = 123L;

    public static final Long STUDENT_COURSE_ID = 1L;

    public static Student createTestStudent() throws NoSuchFieldException, IllegalAccessException{
        return newStudent(STUDENT_ID, STUDENT_NAME, STUDENT_AUTH_HASH);
    }

    public static Student createTestStudent(Long studentId, String studentName, String studentAuthHash) throws NoSuchFieldException, IllegalAccessException{
        return newStudent(studentId, studentName, studentAuthHash);
    }

    public static Student newStudent(Long studentId, String studentName, String studentAuthHash) throws IllegalAccessException, NoSuchFieldException {
        var studentCourses = new HashSet<StudentCourse>();

        Student student = new Student(studentName, studentAuthHash, studentCourses);

        Field privateId = Student.class.getDeclaredField("id");
        privateId.setAccessible(true);

        privateId.set(student, studentId);

        return student;
    }

    public static Course createTestCourse() throws NoSuchFieldException, IllegalAccessException{
        return newCourse(COURSE_ID, COURSE_NAME);
    }

    public static Course createTestCourse(Long courseId, String courseName) throws NoSuchFieldException, IllegalAccessException{
        return newCourse(courseId, courseName);
    }

    public static Course newCourse(Long courseId, String courseName) throws IllegalAccessException, NoSuchFieldException {
        Course course = new Course(courseName, new HashSet<>());

        Field privateId = Course.class.getDeclaredField("id");
        privateId.setAccessible(true);

        privateId.set(course, courseId);

        return course;
    }

    public static StudentCourse createTestStudentCourse() throws NoSuchFieldException, IllegalAccessException {
        return newStudentCourse(STUDENT_COURSE_ID, createTestStudent(), createTestCourse());
    }

    public static StudentCourse createTestStudentCourse(Long studentCourseId, Student student, Course course) throws NoSuchFieldException, IllegalAccessException {
        return newStudentCourse(studentCourseId, student, course);
    }

    public static StudentCourse newStudentCourse(Long studentCourseId, Student student, Course course) throws NoSuchFieldException, IllegalAccessException {
        StudentCourse studentCourse = new StudentCourse(student, course);

        Field privateId = StudentCourse.class.getDeclaredField("id");
        privateId.setAccessible(true);

        privateId.set(studentCourse, studentCourseId);

        return studentCourse;
    }

    public static Set<StudentCourse> createTestStudentCourseSet() throws NoSuchFieldException, IllegalAccessException {
        var studentCourses = new HashSet<StudentCourse>();
        studentCourses.add(createTestStudentCourse());
        return studentCourses;
    }
}
