package com.paurus.students.services;

import com.paurus.students.entities.Course;
import com.paurus.students.entities.Student;

import java.util.List;

public interface SchoolService {
    List<Course> getAllCourses(String searchTerm);

    Student enrollStudentToCourse(Long studentId, Long courseId);

    Student removeStudentFromCourse(Long studentId, Long courseId);
}
