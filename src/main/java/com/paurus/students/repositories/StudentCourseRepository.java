package com.paurus.students.repositories;

import com.paurus.students.entities.StudentCourse;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentCourseRepository extends PagingAndSortingRepository<StudentCourse, Long> {
    void deleteByStudentIdAndCourseId(Long studentId, Long courseId);

    boolean existsByStudentIdAndCourseId(Long studentId, Long courseId);
}
