package com.paurus.students.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StudentDto implements Serializable {

    private Long id;

    private String name;

    private List<CourseDto> courses;

    public StudentDto() {
        this.courses = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCourse(CourseDto course) {
        this.courses.add(course);
    }

    public List<CourseDto> getCourses() {
        return this.courses;
    }
}
