package com.paurus.students.unit.entities;

import com.paurus.students.entities.Student;
import com.paurus.students.entities.StudentCourse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class StudentTest {
    @Test
    public void testConstructorAndGetters() throws NoSuchFieldException, IllegalAccessException {
        Student student = EntityGenerator.createTestStudent();

        assertEquals(EntityGenerator.STUDENT_ID, student.getId());
        assertEquals(EntityGenerator.STUDENT_NAME, student.getName());
        assertEquals(EntityGenerator.STUDENT_AUTH_HASH, student.getAuthHash());
        assertEquals(new HashSet<StudentCourse>(), student.getCourses());
    }

    @Test
    public void testSetters() throws NoSuchFieldException, IllegalAccessException {
        Student student = EntityGenerator.createTestStudent();

        assertEquals(0, student.getCourses().size());

        student.setName(EntityGenerator.STUDENT_NAME_EDIT);
        student.setAuthHash(EntityGenerator.STUDENT_AUTH_HASH_EDIT);
        student.setCourses(EntityGenerator.createTestStudentCourseSet());

        assertEquals(EntityGenerator.STUDENT_NAME_EDIT, student.getName());
        assertEquals(EntityGenerator.STUDENT_AUTH_HASH_EDIT, student.getAuthHash());
        assertEquals(1, student.getCourses().size());
    }
}
