package com.paurus.students.services;

import com.paurus.students.entities.Student;

public interface AuthService {
    Student getAuthorizedStudent(String authHash);
}
