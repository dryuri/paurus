# Students enrolling in classes

Prepare a really simple student information system. In this system students can enroll to different classes. With this system student can:

* list all available classes
* basic search of classes (i.e. by class name)
* student can enroll to a one or many classes
* student can cancel enrollment to a class
* authentication    
    * each student has a unique hash that is used for authentication
    * each operation checks hash for user authenticity

Based on described features prepare a REST service(s) over which this operations can be done. Since your doing a service you don't need to bother with UI. The correctness of implementation can be shown with unit/integration tests or simple Postman (www.getpostman.com) calls. Technologies: Spring, Spring Boot, Maven... how data is stored is up to you :)

# Prerequisites

TODO

# TL; DR

```
TODO quick start here
```

# Run locally

## Setup

TODO

## Swagger

TODO

# Run tests

## Run tests

TODO

## Analyze reports

TODO

# Possible improvements

* If student and course management would be updated with other CRUD operations
we would need to add cache invalidation.
* Search query is very basic since we only have one field to search by. If entity
we are searching through would have more searchable fields we would need to extend 
search functionality.
