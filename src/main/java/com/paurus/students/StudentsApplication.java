package com.paurus.students;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages = {
		"com.paurus.students.controllers",
		"com.paurus.students.services",
		"com.paurus.students.repositories",
		"com.paurus.students.entities",
		"com.paurus.students.dtos",
		"com.paurus.students.exceptions",
		"com.paurus.students.interceptors"
})
@EnableJpaRepositories(basePackages = "com.paurus.students.repositories", repositoryImplementationPostfix = "Impl")
public class StudentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
