package com.paurus.students.services;

import com.paurus.students.entities.Student;
import com.paurus.students.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final StudentRepository studentRepository;

    @Autowired
    public AuthServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Cacheable(value = "authStudentsCache")
    @Override
    public Student getAuthorizedStudent(String authHash) {
        return this.studentRepository.findByAuthHash(authHash);
    }
}
