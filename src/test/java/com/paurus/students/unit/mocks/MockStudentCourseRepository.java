package com.paurus.students.unit.mocks;

import com.paurus.students.entities.StudentCourse;
import com.paurus.students.repositories.StudentCourseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class MockStudentCourseRepository implements StudentCourseRepository {

    private Set<StudentCoursePair> studentcourse;

    public MockStudentCourseRepository() {
        this.studentcourse = new HashSet<>();
    }

    @Override
    public <S extends StudentCourse> S save(S s) {
        this.studentcourse.add(new StudentCoursePair(s.getStudent().getId(), s.getCourse().getId()));
        return s;
    }

    @Override
    public void deleteByStudentIdAndCourseId(Long studentId, Long courseId) {
        this.studentcourse.remove(new StudentCoursePair(studentId, courseId));
    }

    @Override
    public boolean existsByStudentIdAndCourseId(Long studentId, Long courseId) {
        return this.studentcourse.contains(new StudentCoursePair(studentId, courseId));
    }

    @Override
    public Iterable<StudentCourse> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<StudentCourse> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StudentCourse> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<StudentCourse> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<StudentCourse> findAll() {
        return null;
    }

    @Override
    public Iterable<StudentCourse> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(StudentCourse studentCourse) {

    }

    @Override
    public void deleteAll(Iterable<? extends StudentCourse> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    private class StudentCoursePair {
        private Long studentId;
        private Long courseId;

        public StudentCoursePair(Long studentId, Long courseId) {
            this.studentId = studentId;
            this.courseId = courseId;
        }

        public Long getStudentId() {
            return studentId;
        }

        public void setStudentId(Long studentId) {
            this.studentId = studentId;
        }

        public Long getCourseId() {
            return courseId;
        }

        public void setCourseId(Long courseId) {
            this.courseId = courseId;
        }
    }
}
