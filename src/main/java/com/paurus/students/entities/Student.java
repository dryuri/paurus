package com.paurus.students.entities;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "Student")
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String authHash;

    @OneToMany(mappedBy = "student")
    private Set<StudentCourse> studentCourses;

    public Student() {

    }

    public Student(String name, String authHash, Set<StudentCourse> studentCourses) {
        this.name = name;
        this.authHash = authHash;
        this.studentCourses = studentCourses;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthHash() {
        return authHash;
    }

    public void setAuthHash(String authHash) {
        this.authHash = authHash;
    }

    public Set<StudentCourse> getCourses() {
        return studentCourses;
    }

    public void setCourses(Set<StudentCourse> courses) {
        this.studentCourses = courses;
    }
}
