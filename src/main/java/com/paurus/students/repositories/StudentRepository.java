package com.paurus.students.repositories;

import com.paurus.students.entities.Student;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {
    Student findByAuthHash(String authHash);
}
