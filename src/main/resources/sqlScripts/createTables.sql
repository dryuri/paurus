CREATE TABLE student(
  id INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100),
  auth_hash VARCHAR(100),
  PRIMARY KEY(id)
);

CREATE TABLE course(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100),
  PRIMARY KEY(id)
);

CREATE TABLE student_course(
  id INT NOT NULL AUTO_INCREMENT,
  student_id INT NOT NULL,
  course_id INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (student_id) REFERENCES student(id),
  FOREIGN KEY (course_id) REFERENCES course(id),
  CONSTRAINT UC_student_course UNIQUE(student_id,course_id)
);

INSERT INTO course(name) VALUE ('Java for Dummies');
INSERT INTO course(name) VALUE ('Never stop learning: Code for fun');
INSERT INTO course(name) VALUE ('Game development for kids');
INSERT INTO course(name) VALUE ('Living in the Cloud - An In-depth AWS course');
INSERT INTO course(name) VALUE ('Build your business "Just realease it already"');

INSERT INTO student(name, auth_hash) VALUES ('Jurij', 'jurij-hash');
INSERT INTO student(name, auth_hash) VALUES ('Paurus', 'paurus-hash');