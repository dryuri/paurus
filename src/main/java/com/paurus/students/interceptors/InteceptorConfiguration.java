package com.paurus.students.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class InteceptorConfiguration implements WebMvcConfigurer {

    private final AuthInterceptor authInterceptor;

    @Autowired
    public InteceptorConfiguration(AuthInterceptor authInterceptor) {
        this.authInterceptor = authInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.authInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/error", "/error/", "/courses", "/courses/", "/swagger-ui/**", "/v3/**");
    }
}
